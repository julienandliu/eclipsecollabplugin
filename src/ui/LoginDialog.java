package ui;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

public class LoginDialog extends TitleAreaDialog {

  private Text emailAddressText;
  private Text passwordText;

  private String emailAddress;
  private String password;

  public LoginDialog(Shell parentShell) {
    super(parentShell);
  }

  @Override
  public void create() {
    super.create();
    setTitle("Login");
    setMessage("You are not currently logged in. Please log in. If you do not have an account, visit julienandliu.co to make an account.", IMessageProvider.INFORMATION);
  }

  @Override
  protected Control createDialogArea(Composite parent) {
    Composite area = (Composite) super.createDialogArea(parent);
    Composite container = new Composite(area, SWT.NONE);
    container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
    GridLayout layout = new GridLayout(2, false);
    container.setLayout(layout);

    createEmailAddress(container);
    createPassword(container);

    return area;
  }

  private void createEmailAddress(Composite container) {
    Label emailAddressLabel = new Label(container, SWT.NONE);
    emailAddressLabel.setText("Email Address");

    GridData dataEmailAddress = new GridData();
    dataEmailAddress.grabExcessHorizontalSpace = true;
    dataEmailAddress.horizontalAlignment = GridData.FILL;

    emailAddressText = new Text(container, SWT.BORDER);
    emailAddressText.setLayoutData(dataEmailAddress);
  }
  
  private void createPassword(Composite container) {
    Label labelPassword = new Label(container, SWT.NONE);
    labelPassword.setText("Password");
    
    GridData dataPassword = new GridData();
    dataPassword.grabExcessHorizontalSpace = true;
    dataPassword.horizontalAlignment = GridData.FILL;
    passwordText = new Text(container, SWT.PASSWORD | SWT.BORDER);
    passwordText.setLayoutData(dataPassword);
  }



  @Override
  protected boolean isResizable() {
    return true;
  }

  // save content of the Text fields because they get disposed
  // as soon as the Dialog closes
  private void saveInput() {
    emailAddress = emailAddressText.getText();
    password = passwordText.getText();

  }

  @Override
  protected void okPressed() {
    saveInput();
    super.okPressed();
  }

  public String getEmailAddress() {
    return emailAddress;
  }

  public String getPassword() {
    return password;
  }
} 
