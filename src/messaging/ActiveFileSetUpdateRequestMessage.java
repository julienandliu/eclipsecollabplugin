package messaging;

public class ActiveFileSetUpdateRequestMessage {
	
	private String action;
	private int projectId;
	private int userId;
	private int fileId;
	
	public ActiveFileSetUpdateRequestMessage(String action, int projectId, int userId, int fileId) {
		this.action = action;
		this.projectId = projectId;
		this.userId = userId;
		this.fileId = fileId;
	}

}
