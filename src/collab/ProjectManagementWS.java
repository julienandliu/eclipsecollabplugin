package collab;

import java.util.HashMap;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;

import com.google.gson.Gson;

import messaging.FileContentResponseMessage;

@ClientEndpoint
public class ProjectManagementWS extends WebSocket {
	
	private HashMap<String, FileContentResponseMessage> inboundMessages;
	private Gson gson = new Gson();

	public ProjectManagementWS(String destination, HashMap<String, FileContentResponseMessage> inboundMessages) {
		this.destination = destination;
		this.inboundMessages = inboundMessages;
	}
	
	/**
	 * Callback hook for Message Events. This method will be invoked when a client send a message.
	 */
	@Override
	@OnMessage
	public void onMessage(String message) {
		
		System.out.println("In onMessage - ProjectManagementWS");
		
		// Process the message here
		FileContentResponseMessage response = gson.fromJson(message, FileContentResponseMessage.class);
		
		String requestID = response.getRequestID();
		
		inboundMessages.put(requestID, response);
		
	}
	
	public FileContentResponseMessage getResponseMessage(String requestID) {
		while(!inboundMessages.containsKey(requestID)) {
			// do nothing - response not yet arrived.
		}
		
		FileContentResponseMessage response = inboundMessages.get(requestID);
		inboundMessages.remove(requestID);
		
		return response;
	}
	
	
	
}
