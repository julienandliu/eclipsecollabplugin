package collab.actions;


import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;

/**
 * This is the handler for when a user right clicks a project and wishes to disconnect from collaboration.
 * This is intended for disconnecting from collaboration, and continuing to make the changes locally.
 */
public class DisconnectFromCollaborationHandler extends AbstractHandler {
    /**
     * The constructor.
     */
    public DisconnectFromCollaborationHandler() {
    }

    /**
     * This method is executed when menu item "Connect to Project" has been clicked from the application menu.
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        MessageDialog.openInformation(
                window.getShell(),
                "EclipseCollabPlugin",
                "Hello, Eclipse world");
        return null;
    }
}
