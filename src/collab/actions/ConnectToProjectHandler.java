package collab.actions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPSClient;
import org.bson.Document;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;

import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import collab.Activator;
import collab.EclipseCollabSingleton;
import collab.FTPUtil;
import ui.LoginDialog;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.window.Window;
/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @see org.eclipse.core.commands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class ConnectToProjectHandler extends AbstractHandler {

	private EclipseCollabSingleton main;
	String server = "52.203.246.100";
	String user = "hello";
	String pass = "world";
	FTPSClient ftpClient = new FTPSClient(true);

	// Mongo connection
	//private final String MONGO_HOST = "localhost";
	private final String MONGO_HOST = "52.203.246.100";
	private final int MONGO_PORT = 27017;

	/**
	 * The constructor.
	 */
	public ConnectToProjectHandler() {
	}

	/**
	 * This method is executed when menu item "Connect to Project" has been 
	 * clicked from the application menu.
	 */
	public Object execute(ExecutionEvent event) throws ExecutionException {
		IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);

		LoginDialog loginDialog = new LoginDialog(window.getShell());
		loginDialog.create();

		if (main == null) {
			main = Activator.getDefault().getSingleton();
		}

		// Check if we have the user's credentials:
		if (!main.checkCredentialsStored()) {
			// Get user to input credentials
			if (loginDialog.open() == Window.OK) {
				main.setUserCredentials(loginDialog.getEmailAddress(), loginDialog.getPassword());
			} else {
				return null;
			}

		} 

		while(!main.validateCredentials()) {
			MessageDialog.openInformation(
					window.getShell(),
					"EclipseCollabPlugin",
					new String("Problem with account details, try again"));

			if (loginDialog.open() == Window.OK) {
				main.setUserCredentials(loginDialog.getEmailAddress(), loginDialog.getPassword());
			} else {
				return null;
			}
		}

		InputDialog projectNamePrompt = new InputDialog(window.getShell(), 
				"Connect to Existing Project", "Enter name of project to connect to", "", null);
		
		//projectNamePrompt.open();
		
		if (projectNamePrompt.open() == Window.OK) {
			// Get the value entered by the user
			String projectName = projectNamePrompt.getValue();
			
			String projectID = getProjectID(projectName);
			try {
				main.putProjectNameToIDMapping(projectName, Integer.parseInt(projectID));
			} catch (Exception e) {
				System.out.println("Error mapping projectName to projectId, " + e.getLocalizedMessage());
			}
			
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			String saveDirPath = root.getRawLocation().toString();
			String projectPath = saveDirPath + "/" + projectID;
			String projectPathName = saveDirPath + "/" + projectName;
			
			// The project will be renamed later on so save the path with projectName
			main.setConnectedProject(projectName, projectPathName); 
			
//			TODO: prevent redownloading the project if already in workspace
//			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
//			IProject project = root.getProject(projectName);
//			IProject[] projects = root.getProjects();
			
			
			// Try and connect to the project and download.

			if (projectID != null) {

				if (downloadProject(projectID, projectName, saveDirPath, projectPath)) {
					MessageDialog.openInformation(
							window.getShell(),
							"EclipseCollabPlugin",
							new String("Project successfully downloaded and imported into the workspace"));
				} else {
					MessageDialog.openInformation(
							window.getShell(),
							"EclipseCollabPlugin",
							new String("Problem connecting to the project"));
				}
			} else {
				MessageDialog.openInformation(
						window.getShell(),
						"EclipseCollabPlugin",
						new String("Problem connecting to the project"));
			}
		}

		return null;
	}
	

	/**
	 * Return the project name so it can be accessed from the server's 
	 * eclipse workspace
	 * @param projectName
	 * @return
	 */
	private String getProjectID(String projectName) {

		MongoClient mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);
		MongoDatabase db = mongoClient.getDatabase("testdb");

		int userID = main.getUserID();

		List<BasicDBObject> or = new ArrayList<BasicDBObject>();
		BasicDBObject orQuery = new BasicDBObject();
		or.add(new BasicDBObject("collaborators", userID));
		or.add(new BasicDBObject("ownerId", userID));
		orQuery.put("$or", or);

		List<BasicDBObject> and = new ArrayList<BasicDBObject>();
		BasicDBObject andQuery = new BasicDBObject();
		and.add(orQuery);
		and.add(new BasicDBObject("projectName", projectName));
		andQuery.put("$and", and);

		Integer projectID = null;
		FindIterable<org.bson.Document> iterable =  db.getCollection("projects").find(andQuery);

		if (!iterable.iterator().hasNext()) {
			System.out.println("User is not the owner or collaborator of a project by the given name");
			mongoClient.close();
			return null;
		}

		for (Document current : iterable) {
			projectID = current.getInteger("_id");
		}
		
		mongoClient.close();
		return Integer.toString(projectID);
	}

	// Use FTP Client example to download the project to the local workspace
	private boolean downloadProject(String projectID, String projectName, String saveDirPath, String projectPath) {

		try {
			// connect and login to the server
			ftpClient.connect(server);
			ftpClient.login(user, pass);
			
			 // Set protection buffer size
            ftpClient.execPBSZ(0);
            // Set data channel protection to private
            ftpClient.execPROT("P");

			// use local passive mode to pass firewall
            ftpClient.enterLocalPassiveMode();

			System.out.println("Connected");

			String remoteDirPath = "workspace";
			// Download the project
			FTPUtil.downloadDirectory(ftpClient, remoteDirPath, projectID, saveDirPath, true);

			// Import the project into the workspace
			String projectFile = projectPath + "/.project";

			try {
				IProjectDescription description = ResourcesPlugin.getWorkspace().loadProjectDescription(  new Path(projectFile));
				IProject project = ResourcesPlugin.getWorkspace().getRoot().getProject(description.getName());

				project.create(description, null);
				project.open(null);
				
				IProjectDescription desc = project.getDescription();
				desc.setName(projectName);
				project.move(desc, true, null);

				project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
								
			} catch (Exception e) {
				System.out.println("Error importing project");
				e.printStackTrace();
				return false;
			}

			// log out and disconnect from the server
			ftpClient.logout();
			ftpClient.disconnect();

			System.out.println("Disconnected");
			return true;

		} catch (IOException ex) {
			ex.printStackTrace();
			return false;
		}

	}



}

