package collab.actions;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.jface.dialogs.MessageDialog;

/**
 * This is the handler for when a user right clicks a project and wishes to connect for collaboration.
 * This is intended for use when a user creates a project locally in their workspace, and would like to upload it
 * to their account for collaboration. If the project already exists in the web based server, then they should
 * "connect" to it instead from the application menu.
 */
public class ConnectForCollaborationHandler extends AbstractHandler {
    /**
     * The constructor.
     */
    public ConnectForCollaborationHandler() {
    }

    /**
     * This method is executed when menu item "Connect to Project" has been clicked from the application menu.
     */
    public Object execute(ExecutionEvent event) throws ExecutionException {
        IWorkbenchWindow window = HandlerUtil.getActiveWorkbenchWindowChecked(event);
        MessageDialog.openInformation(
                window.getShell(),
                "EclipseCollabPlugin",
                "Hello, Eclipse world");
        return null;
    }
}
