package collab;

import java.util.HashMap;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.swt.widgets.Display;

import collab.ot.ClientManager;
import collab.ot.TextOperation;

public class IDEManager {

	// Map file id -> file content
	private HashMap<Integer, String> openedProjectFiles;
	// Keep track of the client manager for each file
	private HashMap<Integer, ClientManager> fileClients;
	
	// Keep track of the document currently broughtToTop and its listener
	private IDocument currentDocument = null;
	private IDEDocumentListener activeDocumentListener = new IDEDocumentListener();

	public IDEManager() {
		openedProjectFiles = new HashMap<>();
		fileClients = new HashMap<>();
	}
	
	public void insertChangesInEditor(TextOperation operation) {
		int fileId = Activator.getDefault().getSingleton().getCurrentlyOpenFileId();
		String document = getContentForFileId(fileId);

        int offset = 0;

        for (Object op : operation.getOps()) {

            if (operation.isRetain(op)) {
                System.out.println("retain: " + op.toString());
                offset += (int) op;
                
            } else if (operation.isInsert(op)) {
                System.out.println("insert: " + op.toString());

				updateEditor(offset, 0, (String) op);

				offset += ((String) op).length();
				
            } else if (operation.isDelete(op)) {
                System.out.println("delete: " + op.toString());

				updateEditor(offset, -((int) op), "");
                
               	offset += (int) op; // delete is < 0
               	
            } else {
                // Shouldn't get here
                System.out.println("SHOULD NEVER GET THERE - Unknown op type, op: " + op.toString());
            }
        }
	}
	
	/**
	 * Apparently it can only be updated in a different thread with Display.getDefault etc...
	 * 
	 * @param offset
	 * @param length
	 * @param text
	 */
	private void updateEditor(int offset, int length, String text) {
		currentDocument.removeDocumentListener(activeDocumentListener);

		Display.getDefault().syncExec(new Runnable() { // blocking
		    public void run() {
		    	try {
					currentDocument.replace(offset, length, text);
				} catch (BadLocationException ble) {
					System.out.println("Error inserting text in editor, " + ble.getLocalizedMessage());
				}
		    }
		});
		
		currentDocument.addDocumentListener(activeDocumentListener);
	}
	
	public void storeContentForFileId(int fileId, String fileContent) {
		openedProjectFiles.put(fileId, fileContent);
	}
	
	public String getContentForFileId(int fileId) {
		return openedProjectFiles.get(fileId);
	}
	
	public void storeClientForFileId(int fileId, ClientManager client) {
		fileClients.put(fileId, client);
	}
	
	public ClientManager getClientForFileId(int fileId) {
		return fileClients.get(fileId);
	}
	
	public void setCurrentDocument(IDocument doc) {
		if(currentDocument != null) {
			currentDocument.removeDocumentListener(activeDocumentListener);
		}
		currentDocument = doc;
		currentDocument.addDocumentListener(activeDocumentListener);
	}
	
	public IDocument getCurrentDocument() {
		return currentDocument;
	}
}
