package collab;

import java.net.URI;
import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;

@ClientEndpoint
public abstract class WebSocket {

	// Private instance variables
	protected Session userSession;
	protected String destination;

	public WebSocket() {
	
	}

	// Returns a true if connection is successful
	public boolean connect() {
		URI endpointURI = URI.create(destination);
		
		if (userSession != null && userSession.isOpen()) {
			disconnect();
		}

		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();
			container.connectToServer(this, endpointURI);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	// Returns true if disconnection is successful (only return false if there is an error)
	public boolean disconnect() {
		
		try {
			if (userSession.isOpen()) {
				userSession.close();
				return true;
			} else {
				
				// no active session
				System.out.println("Session is not active");
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}


	/**
	 * Callback hook for Connection open events.
	 *
	 * @param userSession the userSession which is opened.
	 */
	@OnOpen
	public void onOpen(Session userSession) {
		System.out.println("opening websocket to " + destination);
		this.userSession = userSession;
	}

	/**
	 * Callback hook for Connection close events.
	 *
	 * @param userSession the userSession which is getting closed.
	 * @param reason the reason for connection close
	 */
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		System.out.println("closing websocket to " + destination);
		this.userSession = null;
	}

	/**
	 * Callback hook for Message Events. This method will be invoked when a client send a message.
	 */
	@OnMessage
	public void onMessage(String message) {
		// Process the message here
		// Other classes to override
	}


	/**
	 * Send a message to the websocket
	 */
	public void sendMessage(String message) {
		this.userSession.getAsyncRemote().sendText(message);
	}
	
	/**
	 * Check if the socket is currently connected.
	 * @return
	 */
	public boolean isConnected() {
		return userSession.isOpen();
	}


}
