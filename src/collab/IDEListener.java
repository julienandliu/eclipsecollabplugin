package collab;

import java.util.UUID;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.dialogs.IPageChangedListener;
import org.eclipse.jface.dialogs.PageChangedEvent;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.part.EditorPart;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.google.gson.Gson;

import collab.ot.ClientManager;
import messaging.ActiveFileSetUpdateRequestMessage;
import messaging.FileContentRequestMessage;
import messaging.FileContentResponseMessage;
import misc.Project;

/**
 * This is the main thread that listens to events on the user interface of the IDE, 
 * and performs actions accordingly.
 * @author Lisa
 *
 */
public class IDEListener implements IPartListener2, IPageChangedListener, IResourceChangeListener {

	private EclipseCollabSingleton main;
	private Gson gson = new Gson();

	// Mongo connection
	//private final String MONGO_HOST = "localhost";
	//	private final String MONGO_HOST = "52.203.246.100";
	//	private final int MONGO_PORT = 27017;

	public IDEListener() {
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.PRE_DELETE);
		ResourcesPlugin.getWorkspace().addResourceChangeListener(this, IResourceChangeEvent.POST_CHANGE);
		main = Activator.getDefault().getSingleton();
	}

	@Override
	public void partActivated(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub


	}

	@Override
	public void partBroughtToTop(IWorkbenchPartReference arg0) {
		IWorkbenchPart part = arg0.getPart(true);

		String fileLocation = null;
		String projectName = null;
		String projectPath = null;

		if (part instanceof IEditorPart) {
			if (((IEditorPart) part).getEditorInput() instanceof IFileEditorInput) {
				IFile file = ((IFileEditorInput) ((EditorPart) part).getEditorInput()).getFile();
				projectName = file.getProject().getName();
				projectPath = file.getProject().getLocation().toString();
				fileLocation = file.getLocation().toString();
				System.out.println("File location: " + file.getLocation());
			}
		} else {
			return;
		}

		fileLocation = fileLocation.replace(projectPath, "");

		if ((projectName != null) && (fileLocation != null)) {

			// Set it as the currently active project - necessary if user open a file from another project
			main.setConnectedProject(projectName, projectPath); 

			int userId = main.getUserID();
			int projectId = main.getProjectID(projectName);
			int fileId = main.getFileID(fileLocation);

			// Let the synchronisation controller know that this file is active
			String toSend = gson.toJson(new ActiveFileSetUpdateRequestMessage("activeFileSet", projectId, userId, fileId));
			main.getRealTimeWS().sendMessage(toSend);

			// Set the currently open fileId
			main.setCurrentlyOpenFileId(fileId);

			// Keep track of the document and add a DocumentListener to it
			IEditorPart ip = (IEditorPart)part;
			ITextEditor ie = (ITextEditor)ip;
			IDocumentProvider dp = ie.getDocumentProvider();
			IDocument doc = dp.getDocument(ie.getEditorInput());

			Activator.getDefault().getIDEManager().setCurrentDocument(doc);
			
			IDEManager ide = Activator.getDefault().getIDEManager();
			String content = ide.getContentForFileId(fileId);
			doc.set(content);
		}

	}

	/**
	 * This method needs to be implemented, because  otherwise we get duplicate
	 * notifications from node because we never deregistered.
	 */
	@Override
	public void partClosed(IWorkbenchPartReference arg0) {

		String projectName = null;

		IWorkbenchPart part = arg0.getPart(true);

		if (part instanceof IEditorPart) {
			if (((IEditorPart) part).getEditorInput() instanceof IFileEditorInput) {
				IFile file = ((IFileEditorInput) ((EditorPart) part).getEditorInput()).getFile();
				projectName = file.getProject().getName();
			}
		}

		int userId = main.getUserID();
		int currentFileId = main.getCurrentlyOpenFileId();
		int projectId = main.getProjectID(projectName);

		JsonObject obj = new JsonObject();
		obj.add("action", "fileClosed");
		obj.add("projectId", projectId);
		obj.add("userId", userId);
		obj.add("fileId", currentFileId);

		// send message to node
		main.getRealTimeWS().sendMessage(obj.toString());

	}

	@Override
	public void partDeactivated(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void partHidden(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void partInputChanged(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Method called when the user open a file in the project structure,
	 * is not called when clicking on tabs
	 */
	@Override
	public void partOpened(IWorkbenchPartReference arg0) {
		IWorkbenchPart part = arg0.getPart(true);

		System.out.println("In part Opened");

		String projectName = null;
		String fileLocation = null;
		String projectPath = null;

		if (part instanceof IEditorPart) {
			if (((IEditorPart) part).getEditorInput() instanceof IFileEditorInput) {
				IFile file = ((IFileEditorInput) ((EditorPart) part)
						.getEditorInput()).getFile();

				projectName = file.getProject().getName();
				fileLocation = file.getLocation().toString();
				projectPath = file.getProject().getLocation().toString();

				System.out.println("File location: " + file.getLocation());
				System.out.println("Project that file belongs to: " + file.getProject().getName());
			}
		} else {
			return;
		}

		//String projectPath = main.getCurrentlyConnectedProject().getProjectPath();
		fileLocation = fileLocation.replace(projectPath, "");

		// fetch the contents of the file from the web, and replace the editor with it
		if ((projectName != null) && (fileLocation != null)) {

			int userId = main.getUserID();
			int projectId = main.getProjectID(projectName);
			int fileId = main.getFileID(fileLocation);

			// Generate a random requestID
			String requestID = generateRandomRequestID();

			// Send request to the project management websocket
			String toSend = gson.toJson(new FileContentRequestMessage("getFileContent", requestID, userId, projectId, fileId));

			// Wait for the response
			main.getProjectManagementWS().sendMessage(toSend);

			// Extract the content, 
			FileContentResponseMessage response = main.getProjectManagementWS().getResponseMessage(requestID);
			String content = response.getFileContent();

			// Store the content
			IDEManager ide = Activator.getDefault().getIDEManager();
			ide.storeContentForFileId(fileId, content);

			// Create a new ClientManager for this file
			ClientManager newClient = new ClientManager(fileId, response.getRevision());
			ide.storeClientForFileId(fileId, newClient);		    

			// Set content in editor
			IEditorPart ip = (IEditorPart)part;
			ITextEditor ie = (ITextEditor)ip;
			IDocumentProvider dp = ie.getDocumentProvider();
			IDocument doc = dp.getDocument(ie.getEditorInput());
			doc.set(content);
		}	
	}

	@Override
	public void partVisible(IWorkbenchPartReference arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public void pageChanged(PageChangedEvent arg0) {
		// TODO Auto-generated method stub

	}

	/**
	 * Not going to finish due to time
	 */
	@Override
	public void resourceChanged(IResourceChangeEvent event) {
		IResource rsrc = event.getResource();

		// Resource is a project
		if (rsrc instanceof IProject) {
			String projectName = rsrc.getName();

			try {
				main.removeProjectNameToIDMapping(projectName);
			} catch (Exception e) {
				e.printStackTrace();
			}

			System.out.println("Iproject resource modified");

		}

		if (event.getType() == IResourceChangeEvent.POST_CHANGE) {
			System.out.println("resource was is a file? " );
			System.out.println(rsrc instanceof IFile);

			System.out.println("resource was is a folder? " );
			System.out.println(rsrc instanceof IFolder);
		}

	}

	private String generateRandomRequestID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}



}
