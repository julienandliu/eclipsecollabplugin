package collab;

import javax.websocket.ClientEndpoint;
import javax.websocket.OnMessage;


/**
 * nodejs Client
 * @author Lisa
 *
 */
@ClientEndpoint
public class ProjectFactoryWS extends WebSocket {

	public ProjectFactoryWS(String destination) {
		this.destination = destination;
	}


	/**
	 * Callback hook for Message Events. This method will be invoked when a client send a message.
	 */
	@Override
	@OnMessage
	public void onMessage(String message) {
		// Process the message here
		System.out.println("In onMessage - ProjectManagementWS");
		
	}



}
