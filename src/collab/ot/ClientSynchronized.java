package collab.ot;

public class ClientSynchronized implements ClientState {

	public ClientSynchronized() {

	}
	
	@Override
	public ClientState applyClient(ClientManager client, TextOperation operation) {
		client.sendOperation(client.getRevision(), operation);
		return new ClientAwaitingConfirm(operation);
	}

	@Override
	public ClientState applyServer(ClientManager client, TextOperation operation) {
	    client.applyOperation(client.getFileId(), operation);
	    return this;
	}

	@Override
	public ClientState serverAck(ClientManager client) {
		System.out.println("SOULD NEVER GET HERE - serverAck should not be called on synchronized state");
		return null;
	}

}
