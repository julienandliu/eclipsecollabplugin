package collab.ot;

public interface ClientState {

	public ClientState applyClient(ClientManager client, TextOperation operation);
	
	public ClientState applyServer(ClientManager client, TextOperation operation);
	
	public ClientState serverAck(ClientManager client);
	
}
