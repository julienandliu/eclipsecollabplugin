package collab.ot;

import java.util.ArrayList;

public class ClientAwaitingConfirm implements ClientState {

	private TextOperation outstanding;

	public ClientAwaitingConfirm(TextOperation outstanding) {
		this.outstanding = outstanding;
	}

	@Override
	public ClientState applyClient(ClientManager client, TextOperation operation) {
		return new ClientAwaitingWithBuffer(this.outstanding, operation);
	}

	@Override
	public ClientState applyServer(ClientManager client, TextOperation operation) {

		ArrayList<TextOperation> pair = TextOperation.transform(this.outstanding, operation);
		
		System.out.println("pair 1 (apply to doc): " + pair.get(1));
		client.applyOperation(client.getFileId(), pair.get(1));
		
		System.out.println("pair 0 (new outstanding): " + pair.get(0));
		return new ClientAwaitingConfirm(pair.get(0));
	}

	@Override
	public ClientState serverAck(ClientManager client) {
		return new ClientSynchronized();
	}

}
