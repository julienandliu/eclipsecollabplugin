package collab.ot;

import java.util.ArrayList;

public class ClientAwaitingWithBuffer implements ClientState {

	private TextOperation outstanding;
	private TextOperation buffer;
	
	public ClientAwaitingWithBuffer(TextOperation outstanding, TextOperation buffer) {
		this.outstanding = outstanding;
		this.buffer = buffer;
	}
	
	@Override
	public ClientState applyClient(ClientManager client, TextOperation operation) {
		TextOperation newBuffer = this.buffer.compose(operation);
	    return new ClientAwaitingWithBuffer(this.outstanding, newBuffer);
	}

	@Override
	public ClientState applyServer(ClientManager client, TextOperation operation) {

	    ArrayList<TextOperation> pair1 = TextOperation.transform(this.outstanding, operation);
	    ArrayList<TextOperation> pair2 = TextOperation.transform(this.buffer, pair1.get(1));
	    
	    client.applyOperation(client.getFileId(), pair2.get(1));
	    
	    return new ClientAwaitingWithBuffer(pair1.get(0), pair2.get(0));
	}

	@Override
	public ClientState serverAck(ClientManager client) {
	    client.sendOperation(client.getRevision(), this.buffer);
	    return new ClientAwaitingConfirm(this.buffer);
	}

}
