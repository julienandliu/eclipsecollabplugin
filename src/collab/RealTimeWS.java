package collab;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.Session;

import org.apache.commons.net.ftp.FTPSClient;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.NullProgressMonitor;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import collab.ot.TextOperation;
import misc.Project;

@ClientEndpoint
public class RealTimeWS extends WebSocket {
	
	String server = "52.203.246.100";
	String user = "hello";
	String pass = "world";
	FTPSClient ftpClient = new FTPSClient(true);

	public RealTimeWS(String destination) {
		this.destination = destination;
	}

	/**
	 * Callback hook for Message Events. This method will be invoked when a client send a message.
	 */
	@Override
	@OnMessage
	public void onMessage(String message) {
		// Process the message here
		JsonObject object = Json.parse(message).asObject();
		String action = object.get("action").asString();
		
		/**
		 * Possible actions:
		 * initConnection, operation, notification, compilation
		 */
		switch(action) {
		case "notification":
			handleNotification(object);
			break;
		case "operation":
			handleOperation(object);
			break;
		default:
			break;

		}
	}

	/**
	 * JSON objects are of the form:
	 * {
	 * 	"action": "notification",
	 * 	"type": "projectStructureUpdate",
	 * [ "byUser": user name,
	 *	"collaboratorsConnected": [collaborators],
	 *	"userID": userID,
	 *	"fileID": fileID  ]  --> optional fields depending on notification type
	 * @param object
	 */
	private void handleNotification(JsonObject object) {

		String type = object.get("type").asString();

		switch (type) {
		case "projectStructureUpdate":
			new Thread(() -> synchroniseProjectStructure()).start();
			break;
		case "collaboratorsConnected":
			//TODO?
			break;
		case "activeFileSet":
			//TODO?
			break;
		default:
			break;
		}

	}

	private void handleOperation(JsonObject msgJson) {
		TextOperation operation = new TextOperation();
		operation.fromJson(msgJson.get("operation").asArray());
		int fileId = msgJson.get("fileId").asInt();
		int userId =  msgJson.get("userId").asInt();
		
		System.out.println("received operation: " + msgJson.get("operation").toString() + " from user: " + userId);
        
		IDEManager ide = Activator.getDefault().getIDEManager();
		EclipseCollabSingleton main = Activator.getDefault().getSingleton();
		
		if (userId == main.getUserID()) {
			ide.getClientForFileId(fileId).serverAck();
        } else {
        	ide.getClientForFileId(fileId).applyServer(operation);
        }
		
	}

	/**
	 * This notification is received when a user has added
	 * or removed something in the project structure
	 * (eg. a folder/package/class). Currently the 
	 */
	private void synchroniseProjectStructure() {

		// Get the currently connected project
		Project currentlyConnectedProject = Activator.getDefault().getSingleton().getCurrentlyConnectedProject();

		// Download the project
		try {
			// connect and login to the server
			ftpClient.connect(server);
			ftpClient.login(user, pass);

			// Set protection buffer size
			ftpClient.execPBSZ(0);
			// Set data channel protection to private
			ftpClient.execPROT("P");

			// use local passive mode to pass firewall
			ftpClient.enterLocalPassiveMode();

			// Get the workspace name
			IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			IProject project = root.getProject(currentlyConnectedProject.getProjectName());
			String remoteDirPath = "workspace";
			String saveDirPath = root.getRawLocation().toString() + "/" + currentlyConnectedProject.getProjectName();
			String projectID = Integer.toString(currentlyConnectedProject.getProjectID());


			FTPUtil.updateDirectory(ftpClient, remoteDirPath, projectID, saveDirPath, true);
			
			project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
			
			// log out and disconnect from the server
			ftpClient.logout();
			ftpClient.disconnect();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Callback hook for Connection close events.
	 *
	 * @param userSession the userSession which is getting closed.
	 * @param reason the reason for connection close
	 */
	@Override
	@OnClose
	public void onClose(Session userSession, CloseReason reason) {
		System.out.println("closing websocket to " + destination);
		this.userSession = null;
	}

}
