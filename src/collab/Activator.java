package collab;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

import misc.Project;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "Collab"; //$NON-NLS-1$

	// The shared instance
	private static Activator plugin;
	// To manage the state of the plugin
	private EclipseCollabSingleton singleton;
	// To keep track of the state of the IDE
	private IDEManager ide;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		this.singleton = new EclipseCollabSingleton();
		this.ide = new IDEManager();
		
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getPartService().addPartListener(new IDEListener());

	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {

		System.out.println("Sending close connection message to node");

		int userId = singleton.getUserID();
		Project project = singleton.getCurrentlyConnectedProject();
		int currentlyOpenFileId = singleton.getCurrentlyOpenFileId();

		if (project != null) {
			JsonObject obj = new JsonObject();
			obj.add("action", "closeConnection");
			obj.add("userId", userId);
			obj.add("projectId", project.getProjectID());

			JsonArray list = new JsonArray();
			list.add(currentlyOpenFileId);

			obj.add("openedFilesId", list);

			// send to websocket
			if (singleton.getRealTimeWS().isConnected()) {
				singleton.getRealTimeWS().sendMessage(obj.toString());
			}
		}

		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}


	public EclipseCollabSingleton getSingleton() {
		return this.singleton;
	}

	public IDEManager getIDEManager() {
		return this.ide;
	}
	
	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}
}
