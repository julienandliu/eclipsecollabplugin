package collab;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.bson.Document;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import messaging.FileContentResponseMessage;
import misc.Project;

/**
 * This class is a singleton that manages the
 * @author Lisa
 *
 */
public class EclipseCollabSingleton {

	// User relateed 
	private String username;
	private String password;
	private int userId;

	// The web sockets
	private ProjectFactoryWS projectFactoryWS;
	private ProjectManagementWS projectManagementWS;
	private RealTimeWS realTimeWS;

	// Websocket address
//		private final String PROJECT_FACTORY_ADDRESS = "ws://localhost:8083";
//		private final String PROJECT_MANAGEMENT_ADDRESS = "ws://localhost:8084";
//		private final String REAL_TIME_ADDRESS = "ws://localhost:8085";

	//Websocket address
	private final String PROJECT_FACTORY_ADDRESS = "ws://52.203.246.100:8083";
	private final String PROJECT_MANAGEMENT_ADDRESS = "ws://52.203.246.100:8084";
	private final String REAL_TIME_ADDRESS = "ws://52.203.246.100:8085";

	// Mongo connection
		//private final String MONGO_HOST = "localhost";
	private final String MONGO_HOST = "52.203.246.100";
	private final int MONGO_PORT = 27017;

	// List of projects for monitoring (to collaborate) when online 
	private Project currentlyConnectedProject;
	private int currentlyOpenFileId;


	// List of projects to their id's (this is persisted between saves)
	private Preferences preferences;
	// Mapping: Project name -> Project ID
	private Preferences projectIDMapping;
	// Mapping: File Path -> File ID
	private Preferences fileIDMapping;

	// Message response queues
	private HashMap<String, FileContentResponseMessage> projectManagementInboundQueue;

	public EclipseCollabSingleton() {
		
		projectManagementInboundQueue = new HashMap<String, FileContentResponseMessage>();
		projectFactoryWS = new ProjectFactoryWS(PROJECT_FACTORY_ADDRESS);
		projectManagementWS = new ProjectManagementWS(PROJECT_MANAGEMENT_ADDRESS, projectManagementInboundQueue);
		realTimeWS = new RealTimeWS(REAL_TIME_ADDRESS);
		currentlyConnectedProject = null;
		preferences = ConfigurationScope.INSTANCE.getNode("EclipseCollabPlugin");
		projectIDMapping = preferences.node("projectIDMappings");
		fileIDMapping = preferences.node("fileIDMapping");
		currentlyOpenFileId = -1;

		new Thread(() -> connect()).start();
	}


	public boolean checkConnected() {
		return projectFactoryWS.isConnected() && projectManagementWS.isConnected() && realTimeWS.isConnected();
	}

	/**
	 * Send a message to Synchronisation controller web socket
	 * @param project
	 */
	private void deregisterProjectFromCollaboration(Project project) {
		JsonObject obj = new JsonObject();
		obj.add("action", "closeConnection");
		obj.add("userId", userId);
		obj.add("projectId", project.getProjectID());
		
		JsonArray list = new JsonArray();
		list.add(currentlyOpenFileId);
		
		obj.add("openedFilesId", list);
		
		// send to websocket
		realTimeWS.sendMessage(obj.toString());
	}

	/**
	 * Send a message to synchronisation controller web socket
	 * @param project
	 */
	private void registerProjectForCollaboration(Project project) {
		JsonObject obj = new JsonObject();
		obj.add("action", "initConnection");
		obj.add("requestID", generateRandomRequestID());
		obj.add("userId", userId);
		obj.add("projectId", project.getProjectID());
		
		// send to websocket
		realTimeWS.sendMessage(obj.toString());
		
	}

	public void setConnectedProject(String projectName, String projectPath) {

		// need to check whether this has changed. if it has need to update node
		Project newProject = new Project(projectName, getProjectID(projectName), projectPath);

		if (currentlyConnectedProject == null) {
			currentlyConnectedProject = newProject;
			registerProjectForCollaboration(newProject);
			return;
		}

		if (newProject.equals(currentlyConnectedProject)) {
			//do nothing.
		} else {
			deregisterProjectFromCollaboration(currentlyConnectedProject);
			currentlyConnectedProject = newProject;
			registerProjectForCollaboration(newProject);
		}

	}

	/**
	 * Returns null if project is not associated with a project id.
	 * Assuming that the project id is an integer for now 
	 * @param projectName
	 * @return
	 */
	public int getProjectID(String projectName) {
		//return projectIDMapping.get(projectName, null);
		return projectIDMapping.getInt(projectName, -1);
	}

	public void putProjectNameToIDMapping(String projectName, int projectID) throws BackingStoreException {
		//return projectIDMapping.put(projectName, projectID)
		projectIDMapping.putInt(projectName, projectID);
		projectIDMapping.flush();
	}

	public void removeProjectNameToIDMapping(String projectName) throws BackingStoreException {
		projectIDMapping.remove(projectName);
		projectIDMapping.flush();
	}

	public int getFileID(String filePath) {
		return fileIDMapping.getInt(filePath, -1);
	}

	public void putFilePathToIDMapping(String filePath, int fileID) throws BackingStoreException {
		fileIDMapping.putInt(filePath, fileID);
		fileIDMapping.flush();
	}

	public void removeFilePathToIDMapping(String filePath) throws BackingStoreException {
		fileIDMapping.remove(filePath);
		fileIDMapping.flush();
	}
	
	public boolean isProjectConnected(String projectName) {
		Project toCheck = new Project(projectName, getProjectID(projectName));
		return currentlyConnectedProject.equals(toCheck);
	}

	public boolean connect() {

		try {
			return projectFactoryWS.connect() && projectManagementWS.connect() && realTimeWS.connect();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return false;
	}

	public boolean disconnect() {
		return projectFactoryWS.disconnect() && projectManagementWS.disconnect() && realTimeWS.disconnect();
	}

	// Connect to the mongo database, and query for the user
	public boolean validateCredentials() {
		MongoClient mongoClient = new MongoClient(MONGO_HOST, MONGO_PORT);
		MongoDatabase db = mongoClient.getDatabase("testdb");


		BasicDBObject andQuery = new BasicDBObject();
		List<BasicDBObject> obj = new ArrayList<BasicDBObject>();
		obj.add(new BasicDBObject("email", username));
		obj.add(new BasicDBObject("password", password));
		andQuery.put("$and", obj);

		FindIterable<org.bson.Document> iterable =  db.getCollection("users").find(andQuery);

		if (!iterable.iterator().hasNext()) {
			System.out.println("User doesn't exist");
			mongoClient.close();
			return false;
		} else {
			System.out.println("User exists");
		}

		// Get the user's id
		FindIterable<org.bson.Document> userIDIterable =  db.getCollection("users").find(new Document("email", username));

		for (Document current : userIDIterable) {
			userId = current.getInteger("_id");

		}

		mongoClient.close();
		return true;
	}
	

	/*************************************************
	 * Private helper methods
	 *************************************************/
	
	private String generateRandomRequestID() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}



	/*************************************************
	 * Getter methods
	 *************************************************/

	public String getUsername() {
		return this.username;
	}

	public String getPassword() {
		return this.password;
	}

	public String[] getCredentials() {
		return new String[] {username, password};
	}

	public boolean checkCredentialsStored() {
		return (username != null) && (password != null);
	}

	public ProjectFactoryWS getProjectFactoryWS() {
		return projectFactoryWS;
	}

	public ProjectManagementWS getProjectManagementWS() {
		return projectManagementWS;
	}

	public RealTimeWS getRealTimeWS() {
		return realTimeWS;
	}

	public int getUserID() {
		return userId;
	}

	public Project getCurrentlyConnectedProject() {
		return currentlyConnectedProject;
	}
	
	public int getCurrentlyOpenFileId() {
		return currentlyOpenFileId;
	}


	/*************************************************
	 * Setter methods
	 *************************************************/

	public void setUserCredentials(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	public void setCurrentlyOpenFileId(int fileId) {
		this.currentlyOpenFileId = fileId;
	}

}
