package collab;

import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.IDocumentListener;

import collab.ot.TextOperation;

public class IDEDocumentListener implements IDocumentListener {

	@Override
	public void documentAboutToBeChanged(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * http://help.eclipse.org/kepler/index.jsp?topic=%2Forg.eclipse.platform.doc.isv%2Freference%2Fapi%2Forg%2Feclipse%2Fjface%2Ftext%2FDocumentEvent.html
	 * This is triggered whenever a change is made to the current open file in the texteditor.
	 * @param arg0
	 */
	@Override
	public void documentChanged(DocumentEvent arg0) {
		// TODO Auto-generated method stub
		System.out.println("Change to document has been made");
		System.out.println("offset: " + arg0.getOffset());
		System.out.println("text: " + arg0.getText() + ", text length: " + arg0.getText().length());
		System.out.println("length: " + arg0.getLength());
		
		IDEManager ide = Activator.getDefault().getIDEManager();
		EclipseCollabSingleton main = Activator.getDefault().getSingleton();
		int fileId = main.getCurrentlyOpenFileId();
		String document = ide.getContentForFileId(fileId);

        int characterCountBefore = arg0.getOffset();
        int characterCountAfter = document.length() - arg0.getOffset();
        
        TextOperation operation = new TextOperation();
        
        if (arg0.getLength() == 0) { // insert
            operation.retain(characterCountBefore)
                .insert(arg0.getText())
                .retain(characterCountAfter);
            
        } else if (arg0.getLength() != 0 && arg0.getText().length() == 0){ // delete
            operation.retain(characterCountBefore)
                .delete(arg0.getLength())
                .retain(characterCountAfter - arg0.getLength());
            
        } else if (arg0.getLength() != 0 && arg0.getText().length() != 0) {// replace = delete + insert 
        	operation.retain(characterCountBefore)
        		.insert(arg0.getText())
        		.delete(arg0.getLength())
        		.retain(characterCountAfter - arg0.getLength());
        	
        } else {
        	System.out.println("SHOULD NEVER GET HERE - Unknown operation");
        }

        // Update the document
        ide.storeContentForFileId(fileId, operation.apply(document));
        
        // Will trigger client.sendOperation
        ide.getClientForFileId(fileId).applyClient(operation);
	}

}
