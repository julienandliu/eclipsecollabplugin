package misc;

public class Project {
	
	private String projectName;
	private int projectID;
	private String projectPath;
	
	public Project(String projectName, int projectID, String projectPath) {
		this.projectName = projectName;
		this.projectID = projectID;
		this.projectPath = projectPath;
	}
	
	public Project(String projectName, int projectID) {
		this.projectName = projectName;
		this.projectID = projectID;
	}
	
	public String getProjectName() {
		return projectName;
	}
	
	public int getProjectID() {
		return projectID;
	}
	
	public String getProjectPath() {
		return projectPath;
	}
	
	@Override
	public boolean equals(Object o) {
		if ( o == this) {
			return true;
		}
		
		if (!(o instanceof Project)) {
			return false;
		}
		
		Project project = (Project) o; 
		
		return project.getProjectName().equals(projectName) &&
				project.getProjectID() == projectID &&
				project.getProjectPath().equals(projectPath);
		
	}
	
	@Override
	public int hashCode() {
		int result = 17;
		result = 31 * result + projectName.hashCode();
		result = 31 * result + projectID;
		result = 31 * result + projectPath.hashCode();
		
		return result;
	}

}
